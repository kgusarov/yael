# YAEL #

YAEL stands for Yet Another Esoteric Language and I swear it - it is :)

## Overview ##

YAEL was designed to be unholy verbose and hard-to-read and use. It may not have achieved its second goal but first one should be considered achieved. 
We all know LISP - it's very positive language since it has a lots of smileys. YAEL wants to be a positive one too. Main difference from LISP is that YAEL uses two types of brackets - **()** and **{}**.
Since it is possible to implement Brainfuck interpreter in YAEL it should be Touring-complete (at least with non-limited memory).

## Data Types ##

Currently YAEL supports 3 data types:

* Integer
* String
* List

Every defined variable at given moment of time is of one of those types. YAEL supports primitive type conversions so if we for example use integer **i** in a place where list argument is expected it will be automatically converted to list **(i)**

## Syntax ##

Refer to [**WIKI**](https://bitbucket.org/kgusarov/yael/wiki/Syntax)

## Examples ##

Refer to [**WIKI**](https://bitbucket.org/kgusarov/yael/wiki/Examples)