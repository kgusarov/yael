package yael.compiler;

public class CallableBlock extends CompileUnit {
    public static final String DEFAULT_NAME = "<BLOCK>";

    public CallableBlock() {
        super(DEFAULT_NAME);
    }
}
