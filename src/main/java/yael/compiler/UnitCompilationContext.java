package yael.compiler;

import yael.vm.Operator;

import java.util.ArrayDeque;
import java.util.Deque;

class UnitCompilationContext {
    final Deque<Operator> operators = new ArrayDeque<>();
}
