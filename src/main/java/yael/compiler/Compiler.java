package yael.compiler;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yael.antlr.YAELBaseListener;
import yael.antlr.YAELLexer;
import yael.antlr.YAELParser;
import yael.antlr.YAELParser.*;
import yael.vm.Argument;
import yael.vm.Function;
import yael.vm.Operator;
import yael.vm.operators.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Compiler extends YAELBaseListener {
    private static final Logger LOG = LoggerFactory.getLogger(Compiler.class);

    private final ANTLRInputStream is;
    private final Deque<CompileUnit> compileUnitStack = new ArrayDeque<>();
    private final Deque<UnitCompilationContext> unitContextStack = new ArrayDeque<>();
    private final Deque<OperatorCompilationContext> operatorStack = new ArrayDeque<>();
    private final Program program = new Program(CompileUnit.GLOBAL);
    private int calls = 0;

    private Compiler(ANTLRInputStream is) {
        this.is = is;
    }

    public static Program compile(File sourceFile, boolean trace) {
        try (InputStream is = new FileInputStream(sourceFile)) {
            return new Compiler(new ANTLRInputStream(is)).compile(trace);
        } catch (Exception e) {
            LOG.error("Failed to compile " + sourceFile, e);
        }

        return null;
    }

    public static Program compile(String sourceFile, boolean trace) {
        try (InputStream is = Compiler.class.getClassLoader().getResourceAsStream(sourceFile)) {
            return new Compiler(new ANTLRInputStream(is)).compile(trace);
        } catch (Exception e) {
            LOG.error("Failed to compile " + sourceFile, e);
        }

        return null;
    }

    public Program compile(boolean trace) {
        YAELLexer lexer = new YAELLexer(is);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        YAELParser parser = new YAELParser(tokens);

        parser.setBuildParseTree(true);
        parser.addParseListener(this);
        parser.setTrace(trace);

        parser.program();
        return program;
    }

    private CompileUnit currentCompileUnit() {
        if (compileUnitStack.isEmpty()) {
            return program;
        }

        return compileUnitStack.getLast();
    }

    private OperatorCompilationContext currentOperatorContext() {
        return operatorStack.getLast();
    }

    private UnitCompilationContext currentUnitContext() {
        return unitContextStack.getLast();
    }

    @Override
    public void exitArgumentNameList(ArgumentNameListContext ctx) {
        if (!compileUnitStack.isEmpty()) {
            CompileUnit unit = compileUnitStack.pollLast();

            Stream<String> argumentNames = ctx.variable().stream().map(v -> v.Identifier().getText());
            compileUnitStack.addLast(new Function(unit.getName(), argumentNames.collect(Collectors.toList())));
        }
    }

    @Override
    public void exitFunctionName(FunctionNameContext ctx) {
        if (calls == 0) {
            compileUnitStack.addLast(new CompileUnit(ctx.Identifier().getText()));
        }
    }

    @Override
    public void enterOperator(OperatorContext ctx) {
        operatorStack.add(new OperatorCompilationContext());
    }

    @Override
    public void exitAssign(AssignContext ctx) {
        currentUnitContext().operators.addLast(new OpAssign(getArgumentArray(operatorStack.removeLast())));
    }

    private Argument[] getArgumentArray(OperatorCompilationContext ctx) {
        return ctx.arguments.toArray(new Argument[ctx.arguments.size()]);
    }

    @Override
    public void exitArgument(ArgumentContext ctx) {
        currentOperatorContext().arguments.add(ExpressionFactory.create(ctx));
    }

    @Override
    public void exitExpression(ExpressionContext ctx) {
        Operator op = (ctx.operator() != null) ? currentUnitContext().operators.removeLast() : null;
        currentOperatorContext().arguments.add(ExpressionFactory.create(ctx, op));
    }

    @Override
    public void exitFunction(FunctionContext ctx) {
        if (!compileUnitStack.isEmpty()) {
            CompileUnit unit = compileUnitStack.pollLast();
            currentCompileUnit().add(unit);
        }
    }

    @Override
    public void exitCompileUnit(CompileUnitContext ctx) {
        moveCurrentOperatorsToCurrentCompileUnit();
    }

    @Override
    public void exitCallableBlock(CallableBlockContext ctx) {
        moveCurrentOperatorsToCurrentCompileUnit();
    }

    private void moveCurrentOperatorsToCurrentCompileUnit() {
        currentCompileUnit().add(currentUnitContext().operators);
        unitContextStack.removeLast();
    }

    @Override
    public void exitPrintChar(PrintCharContext ctx) {
        currentUnitContext().operators.addLast(new OpPrintChar(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitReturnOperator(ReturnOperatorContext ctx) {
        currentUnitContext().operators.addLast(new OpReturn(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void enterCall(CallContext ctx) {
        calls++;
    }

    @Override
    public void exitCall(CallContext ctx) {
        currentUnitContext().operators.addLast(new OpCall(ctx.functionName().Identifier().getText(),
                getArgumentArray(operatorStack.removeLast())));
        calls--;
    }

    @Override
    public void exitTargetVariable(TargetVariableContext ctx) {
        currentOperatorContext().arguments.add(ExpressionFactory.create(ctx));
    }

    @Override
    public void exitIfOperator(IfOperatorContext ctx) {
        CompileUnit ifUnit = null;
        CompileUnit elseUnit = null;

        if (ctx.callableBlock().size() == 2) {
            elseUnit = compileUnitStack.removeLast();
            ifUnit = compileUnitStack.removeLast();
        } else {
            ifUnit = compileUnitStack.removeLast();
        }

        currentUnitContext().operators.addLast(new OpIf(ifUnit, elseUnit,
                getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitRepeat(RepeatContext ctx) {
        currentUnitContext().operators.addLast(new OpRepeat(compileUnitStack.removeLast(),
                getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void enterCallableBlock(CallableBlockContext ctx) {
        compileUnitStack.addLast(new CallableBlock());
        unitContextStack.addLast(new UnitCompilationContext());
    }

    @Override
    public void enterCompileUnit(CompileUnitContext ctx) {
        unitContextStack.addLast(new UnitCompilationContext());
    }

    @Override
    public void exitPrintString(PrintStringContext ctx) {
        currentUnitContext().operators.addLast(new OpPrintString(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitSum(SumContext ctx) {
        currentUnitContext().operators.addLast(new OpSum(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitNegate(NegateContext ctx) {
        currentUnitContext().operators.addLast(new OpNegate(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitList(ListContext ctx) {
        currentUnitContext().operators.addLast(new OpList(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitWhileOperator(WhileOperatorContext ctx) {
        currentUnitContext().operators.addLast(new OpWhile(compileUnitStack.removeLast(),
                getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitHead(HeadContext ctx) {
        currentUnitContext().operators.addLast(new OpHead(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitRemoveHead(RemoveHeadContext ctx) {
        currentUnitContext().operators.addLast(new OpRemoveHead(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitPrint(PrintContext ctx) {
        currentUnitContext().operators.addLast(new OpPrint(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitEquals(EqualsContext ctx) {
        currentUnitContext().operators.addLast(new OpEquals(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitAsList(AsListContext ctx) {
        currentUnitContext().operators.addLast(new OpAsList(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitAsInt(AsIntContext ctx) {
        currentUnitContext().operators.addLast(new OpAsInt(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitAsString(AsStringContext ctx) {
        currentUnitContext().operators.addLast(new OpAsString(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitReadInt(ReadIntContext ctx) {
        currentUnitContext().operators.addLast(new OpReadInt(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitReadString(ReadStringContext ctx) {
        currentUnitContext().operators.addLast(new OpReadString(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitAt(AtContext ctx) {
        currentUnitContext().operators.addLast(new OpAt(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitGlobal(GlobalContext ctx) {
        currentUnitContext().operators.addLast(new OpAssignGlobal(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitSetAt(SetAtContext ctx) {
        currentUnitContext().operators.addLast(new OpSetAt(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitRemoveLast(RemoveLastContext ctx) {
        currentUnitContext().operators.addLast(new OpRemoveLast(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitLast(LastContext ctx) {
        currentUnitContext().operators.addLast(new OpLast(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitAdd(AddContext ctx) {
        currentUnitContext().operators.addLast(new OpAdd(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitLength(LengthContext ctx) {
        currentUnitContext().operators.addLast(new OpLength(getArgumentArray(operatorStack.removeLast())));
    }

    @Override
    public void exitNot(NotContext ctx) {
        currentUnitContext().operators.addLast(new OpNot(getArgumentArray(operatorStack.removeLast())));
    }
}
