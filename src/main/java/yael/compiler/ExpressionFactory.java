package yael.compiler;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.commons.lang3.StringEscapeUtils;
import yael.antlr.YAELParser.*;
import yael.vm.*;

public class ExpressionFactory {
    private ExpressionFactory() {
    }

    public static Argument create(TargetVariableContext ctx) {
        VariableContext variable = ctx.variable();
        if (variable == null) {
            invalid(ctx);
        }

        return new Expression(variable.Identifier().getText());
    }

    public static Argument create(ArgumentContext ctx) {
        IntegerContext integer = ctx.integer();
        VariableContext variable = ctx.variable();
        StringContext string = ctx.string();

        return create(ctx, integer, variable, string);
    }

    private static Argument create(ParserRuleContext ctx, IntegerContext integer, VariableContext variable, StringContext string) {
        if ((integer == null) && (variable == null) && (string == null)) {
            invalid(ctx);
        } else if (integer != null) {
            return create(ctx, integer);
        } else if (variable != null) {
            return new Expression(variable.Identifier().getText());
        } else if (string != null) {
            String value = string.StringLiteral().getText();
            value = StringEscapeUtils.unescapeJava(value);
            value = value.substring(1, value.length() - 1);

            return new Expression(new StringVariable(value));
        }

        return null;
    }

    private static Argument create(ParserRuleContext ctx, IntegerContext integer) {
        if (integer.SignedInteger() != null) {
            Variable var = new IntegerVariable(Integer.valueOf(integer.SignedInteger().getText()));
            return new Expression(var);
        } else if (integer.UnsignedInteger() != null) {
            Variable var = new IntegerVariable(Integer.valueOf(integer.UnsignedInteger().getText()));
            return new Expression(var);
        } else {
            invalid(ctx);
        }

        return null;
    }

    public static Argument create(ExpressionContext ctx, Operator op) {
        IntegerContext integer = ctx.integer();
        VariableContext variable = ctx.variable();
        OperatorContext operator = ctx.operator();
        StringContext string = ctx.string();

        if ((integer != null) || (variable != null) || (string != null)) {
            return create(ctx, integer, variable, string);
        }

        if ((operator == null) || (op == null)) {
            invalid(ctx);
        }

        return new Expression(op);
    }

    private static void invalid(ParserRuleContext ctx) {
        throw new IllegalArgumentException("Bad argument context : " + ctx.getText());
    }
}
