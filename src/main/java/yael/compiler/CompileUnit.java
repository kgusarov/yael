package yael.compiler;

import yael.vm.Context;
import yael.vm.EmptyVariable;
import yael.vm.Operator;
import yael.vm.Variable;

import java.util.*;

public class CompileUnit {
    public static final String GLOBAL = "<GLOBAL>";

    protected final String name;
    protected final List<Operator> operators = new ArrayList<>();
    protected final Map<String, CompileUnit> internalCompileUnits = new HashMap<>();

    public CompileUnit(String name) {
        this.name = name;
    }

    public void add(Deque<Operator> operators) {
        this.operators.addAll(operators);
    }

    public void add(CompileUnit cu) {
        internalCompileUnits.put(cu.name, cu);
    }

    public void add(Collection<CompileUnit> units) {
        units.forEach(u -> internalCompileUnits.put(u.name, u));
    }

    public String getName() {
        return name;
    }

    public List<Operator> operators() {
        return operators;
    }

    public CompileUnit unit(String name) {
        return internalCompileUnits.get(name);
    }

    @Override
    public String toString() {
        return toString(0);
    }

    public String toString(int indent) {
        StringBuilder sb = new StringBuilder();
        StringUtil.indent(indent, sb);

        sb.append(getName()).append(" [\n");
        for (Operator o : operators) {
            sb.append('\t').append(o.toString(indent)).append('\n');
        }

        for (CompileUnit cu : internalCompileUnits.values()) {
            sb.append(cu.toString(indent + 1)).append('\n');
        }

        StringUtil.indent(indent, sb);
        return sb.append(']').toString();
    }

    public Variable eval(Context ctx) {
        Variable result = EmptyVariable.EMPTY_INSTANCE;
        for (Operator operator : operators()) {
            result = operator.eval(ctx);
        }

        return result;
    }
}
