package yael.compiler;

public class StringUtil {
    private StringUtil() {
    }

    public static void indent(int indent, StringBuilder sb) {
        for (int i = 0; i < indent; i++) {
            sb.append('\t');
        }
    }
}
