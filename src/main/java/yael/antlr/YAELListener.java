// Generated from yael\antlr\YAEL.g4 by ANTLR 4.2.2
package yael.antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link YAELParser}.
 */
public interface YAELListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link YAELParser#argument}.
	 * @param ctx the parse tree
	 */
	void enterArgument(@NotNull YAELParser.ArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#argument}.
	 * @param ctx the parse tree
	 */
	void exitArgument(@NotNull YAELParser.ArgumentContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(@NotNull YAELParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(@NotNull YAELParser.StringContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#ifOperator}.
	 * @param ctx the parse tree
	 */
	void enterIfOperator(@NotNull YAELParser.IfOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#ifOperator}.
	 * @param ctx the parse tree
	 */
	void exitIfOperator(@NotNull YAELParser.IfOperatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#targetVariableList}.
	 * @param ctx the parse tree
	 */
	void enterTargetVariableList(@NotNull YAELParser.TargetVariableListContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#targetVariableList}.
	 * @param ctx the parse tree
	 */
	void exitTargetVariableList(@NotNull YAELParser.TargetVariableListContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#global}.
	 * @param ctx the parse tree
	 */
	void enterGlobal(@NotNull YAELParser.GlobalContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#global}.
	 * @param ctx the parse tree
	 */
	void exitGlobal(@NotNull YAELParser.GlobalContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#sum}.
	 * @param ctx the parse tree
	 */
	void enterSum(@NotNull YAELParser.SumContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#sum}.
	 * @param ctx the parse tree
	 */
	void exitSum(@NotNull YAELParser.SumContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(@NotNull YAELParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(@NotNull YAELParser.ProgramContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#integer}.
	 * @param ctx the parse tree
	 */
	void enterInteger(@NotNull YAELParser.IntegerContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#integer}.
	 * @param ctx the parse tree
	 */
	void exitInteger(@NotNull YAELParser.IntegerContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#removeLast}.
	 * @param ctx the parse tree
	 */
	void enterRemoveLast(@NotNull YAELParser.RemoveLastContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#removeLast}.
	 * @param ctx the parse tree
	 */
	void exitRemoveLast(@NotNull YAELParser.RemoveLastContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#operator}.
	 * @param ctx the parse tree
	 */
	void enterOperator(@NotNull YAELParser.OperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#operator}.
	 * @param ctx the parse tree
	 */
	void exitOperator(@NotNull YAELParser.OperatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#asList}.
	 * @param ctx the parse tree
	 */
	void enterAsList(@NotNull YAELParser.AsListContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#asList}.
	 * @param ctx the parse tree
	 */
	void exitAsList(@NotNull YAELParser.AsListContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#head}.
	 * @param ctx the parse tree
	 */
	void enterHead(@NotNull YAELParser.HeadContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#head}.
	 * @param ctx the parse tree
	 */
	void exitHead(@NotNull YAELParser.HeadContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#argumentList}.
	 * @param ctx the parse tree
	 */
	void enterArgumentList(@NotNull YAELParser.ArgumentListContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#argumentList}.
	 * @param ctx the parse tree
	 */
	void exitArgumentList(@NotNull YAELParser.ArgumentListContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#asInt}.
	 * @param ctx the parse tree
	 */
	void enterAsInt(@NotNull YAELParser.AsIntContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#asInt}.
	 * @param ctx the parse tree
	 */
	void exitAsInt(@NotNull YAELParser.AsIntContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#not}.
	 * @param ctx the parse tree
	 */
	void enterNot(@NotNull YAELParser.NotContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#not}.
	 * @param ctx the parse tree
	 */
	void exitNot(@NotNull YAELParser.NotContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#whileOperator}.
	 * @param ctx the parse tree
	 */
	void enterWhileOperator(@NotNull YAELParser.WhileOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#whileOperator}.
	 * @param ctx the parse tree
	 */
	void exitWhileOperator(@NotNull YAELParser.WhileOperatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#callableBlock}.
	 * @param ctx the parse tree
	 */
	void enterCallableBlock(@NotNull YAELParser.CallableBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#callableBlock}.
	 * @param ctx the parse tree
	 */
	void exitCallableBlock(@NotNull YAELParser.CallableBlockContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(@NotNull YAELParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(@NotNull YAELParser.FunctionContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#repeat}.
	 * @param ctx the parse tree
	 */
	void enterRepeat(@NotNull YAELParser.RepeatContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#repeat}.
	 * @param ctx the parse tree
	 */
	void exitRepeat(@NotNull YAELParser.RepeatContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(@NotNull YAELParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(@NotNull YAELParser.BlockContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#asString}.
	 * @param ctx the parse tree
	 */
	void enterAsString(@NotNull YAELParser.AsStringContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#asString}.
	 * @param ctx the parse tree
	 */
	void exitAsString(@NotNull YAELParser.AsStringContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#readInt}.
	 * @param ctx the parse tree
	 */
	void enterReadInt(@NotNull YAELParser.ReadIntContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#readInt}.
	 * @param ctx the parse tree
	 */
	void exitReadInt(@NotNull YAELParser.ReadIntContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#add}.
	 * @param ctx the parse tree
	 */
	void enterAdd(@NotNull YAELParser.AddContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#add}.
	 * @param ctx the parse tree
	 */
	void exitAdd(@NotNull YAELParser.AddContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull YAELParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull YAELParser.ExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#last}.
	 * @param ctx the parse tree
	 */
	void enterLast(@NotNull YAELParser.LastContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#last}.
	 * @param ctx the parse tree
	 */
	void exitLast(@NotNull YAELParser.LastContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(@NotNull YAELParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(@NotNull YAELParser.FunctionNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#length}.
	 * @param ctx the parse tree
	 */
	void enterLength(@NotNull YAELParser.LengthContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#length}.
	 * @param ctx the parse tree
	 */
	void exitLength(@NotNull YAELParser.LengthContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#readString}.
	 * @param ctx the parse tree
	 */
	void enterReadString(@NotNull YAELParser.ReadStringContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#readString}.
	 * @param ctx the parse tree
	 */
	void exitReadString(@NotNull YAELParser.ReadStringContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#list}.
	 * @param ctx the parse tree
	 */
	void enterList(@NotNull YAELParser.ListContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#list}.
	 * @param ctx the parse tree
	 */
	void exitList(@NotNull YAELParser.ListContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#returnOperator}.
	 * @param ctx the parse tree
	 */
	void enterReturnOperator(@NotNull YAELParser.ReturnOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#returnOperator}.
	 * @param ctx the parse tree
	 */
	void exitReturnOperator(@NotNull YAELParser.ReturnOperatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#call}.
	 * @param ctx the parse tree
	 */
	void enterCall(@NotNull YAELParser.CallContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#call}.
	 * @param ctx the parse tree
	 */
	void exitCall(@NotNull YAELParser.CallContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#setAt}.
	 * @param ctx the parse tree
	 */
	void enterSetAt(@NotNull YAELParser.SetAtContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#setAt}.
	 * @param ctx the parse tree
	 */
	void exitSetAt(@NotNull YAELParser.SetAtContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#removeHead}.
	 * @param ctx the parse tree
	 */
	void enterRemoveHead(@NotNull YAELParser.RemoveHeadContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#removeHead}.
	 * @param ctx the parse tree
	 */
	void exitRemoveHead(@NotNull YAELParser.RemoveHeadContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#print}.
	 * @param ctx the parse tree
	 */
	void enterPrint(@NotNull YAELParser.PrintContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#print}.
	 * @param ctx the parse tree
	 */
	void exitPrint(@NotNull YAELParser.PrintContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#targetVariable}.
	 * @param ctx the parse tree
	 */
	void enterTargetVariable(@NotNull YAELParser.TargetVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#targetVariable}.
	 * @param ctx the parse tree
	 */
	void exitTargetVariable(@NotNull YAELParser.TargetVariableContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#at}.
	 * @param ctx the parse tree
	 */
	void enterAt(@NotNull YAELParser.AtContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#at}.
	 * @param ctx the parse tree
	 */
	void exitAt(@NotNull YAELParser.AtContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#argumentNameList}.
	 * @param ctx the parse tree
	 */
	void enterArgumentNameList(@NotNull YAELParser.ArgumentNameListContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#argumentNameList}.
	 * @param ctx the parse tree
	 */
	void exitArgumentNameList(@NotNull YAELParser.ArgumentNameListContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#compileUnit}.
	 * @param ctx the parse tree
	 */
	void enterCompileUnit(@NotNull YAELParser.CompileUnitContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#compileUnit}.
	 * @param ctx the parse tree
	 */
	void exitCompileUnit(@NotNull YAELParser.CompileUnitContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#negate}.
	 * @param ctx the parse tree
	 */
	void enterNegate(@NotNull YAELParser.NegateContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#negate}.
	 * @param ctx the parse tree
	 */
	void exitNegate(@NotNull YAELParser.NegateContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#equals}.
	 * @param ctx the parse tree
	 */
	void enterEquals(@NotNull YAELParser.EqualsContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#equals}.
	 * @param ctx the parse tree
	 */
	void exitEquals(@NotNull YAELParser.EqualsContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(@NotNull YAELParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(@NotNull YAELParser.VariableContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#printString}.
	 * @param ctx the parse tree
	 */
	void enterPrintString(@NotNull YAELParser.PrintStringContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#printString}.
	 * @param ctx the parse tree
	 */
	void exitPrintString(@NotNull YAELParser.PrintStringContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#assign}.
	 * @param ctx the parse tree
	 */
	void enterAssign(@NotNull YAELParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#assign}.
	 * @param ctx the parse tree
	 */
	void exitAssign(@NotNull YAELParser.AssignContext ctx);

	/**
	 * Enter a parse tree produced by {@link YAELParser#printChar}.
	 * @param ctx the parse tree
	 */
	void enterPrintChar(@NotNull YAELParser.PrintCharContext ctx);
	/**
	 * Exit a parse tree produced by {@link YAELParser#printChar}.
	 * @param ctx the parse tree
	 */
	void exitPrintChar(@NotNull YAELParser.PrintCharContext ctx);
}