package yael.vm;

import yael.compiler.CompileUnit;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class VM implements Context {
    private final Map<String, Variable> globals = new HashMap<>();
    private final CompileUnit unit;

    public VM(CompileUnit unit) {
        this.unit = unit;
    }

    Variable global(String name) {
        Variable result = globals.get(name);
        return result == null ? EmptyVariable.EMPTY_INSTANCE : result;
    }

    void global(String name, Variable value) {
        globals.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("VM [\n");
        for (Entry<String, Variable> entry : globals.entrySet()) {
            sb.append('\t').append(entry.getKey()).append(" = ").append(entry.getValue()).append('\n');
        }

        return sb.append(']').toString();
    }

    @Override
    public Variable var(String name) {
        return global(name);
    }

    @Override
    public void var(String name, Variable value) {
        global(name, value);
    }

    public Variable run() {
        return unit.eval(this);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends CompileUnit> T unit(String name) {
        CompileUnit result = unit.unit(name);
        if (result == null) {
            invalidUnit(name);
        }

        return (T) result;
    }

    @Override
    public Context global() {
        return this;
    }

    @Override
    public Context parentContext() {
        return null;
    }
}
