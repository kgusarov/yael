package yael.vm;

public class Expression implements Argument {
    private final boolean isIdentifier;
    private final Variable value;
    private final Operator operator;
    private final String identifier;

    public Expression(boolean isIdentifier, Variable value, Operator operator, String identifier) {
        this.isIdentifier = isIdentifier;
        this.value = value;
        this.operator = operator;
        this.identifier = identifier;
    }

    public Expression(Variable value) {
        this(false, value, null, null);
    }

    public Expression(String identifier) {
        this(true, null, null, identifier);
    }

    public Expression(Operator operator) {
        this(false, null, operator, null);
    }

    @Override
    public Variable eval(Context ctx) {
        if (isIdentifier) {
            return ctx.var(identifier);
        }

        if (value != null) {
            return value;
        }

        if (operator == null) {
            throw new IllegalStateException(this + " cannot be evaluated");
        }

        return operator.eval(ctx);
    }

    @Override
    public String identifier() {
        if (!isIdentifier) {
            throw new IllegalStateException(this + " is not an identifier");
        }

        return identifier;
    }

    @Override
    public boolean isIdentifier() {
        return isIdentifier;
    }

    @Override
    public String toString() {
        if (isIdentifier) {
            return identifier;
        }

        if (value != null) {
            return value.toString();
        }

        if (operator != null) {
            return operator.toString();
        }

        return "<UNDEFINED>";
    }
}
