package yael.vm;

public interface Argument extends ValueHolder, IdentifierHolder, IsIdentifier {
}

@FunctionalInterface
interface ValueHolder {
    Variable eval(Context ctx);
}

@FunctionalInterface
interface IdentifierHolder {
    String identifier();
}

@FunctionalInterface
interface IsIdentifier {
    boolean isIdentifier();
}
