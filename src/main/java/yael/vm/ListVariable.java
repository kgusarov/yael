package yael.vm;

public class ListVariable implements Variable {
    private VariableList value;

    public ListVariable(VariableList value) {
        this.value = value;
    }

    @Override
    public boolean isTrue() {
        return !value.isEmpty();
    }

    @Override
    public int asInt() {
        return value.isEmpty() ? 0 : value.get(0).asInt();
    }

    @Override
    public VariableList asList() {
        return value;
    }

    @Override
    public VariableType type() {
        return VariableType.LIST;
    }

    @Override
    public String toString() {
        return "List " + value;
    }

    @Override
    public String asString() {
        return dump();
    }
}
