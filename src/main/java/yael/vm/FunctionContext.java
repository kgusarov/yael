package yael.vm;

import yael.compiler.CompileUnit;

import java.util.HashMap;
import java.util.Map;

public class FunctionContext implements Context {
    final Map<String, Variable> variables = new HashMap<>();
    private final Context parent;
    private final CompileUnit unit;
    Variable result = EmptyVariable.EMPTY_INSTANCE;

    public FunctionContext(Context parent, CompileUnit unit) {
        this.parent = parent;
        this.unit = unit;
    }

    @Override
    public Variable var(String name) {
        Variable result = variables.get(name);
        if (result == null) {
            result = parent.var(name);
        }

        return result;
    }

    @Override
    public void var(String name, Variable value) {
        variables.put(name, value);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends CompileUnit> T unit(String name) {
        CompileUnit result = unit.unit(name);
        if (result == null) {
            result = parent.unit(name);
        }

        return (T) result;
    }

    @Override
    public Context global() {
        Context result = this;
        while (result.parentContext() != null) {
            result = result.parentContext();
        }

        return result;
    }

    @Override
    public Context parentContext() {
        return parent;
    }
}