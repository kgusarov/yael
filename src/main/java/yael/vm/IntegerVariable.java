package yael.vm;

public class IntegerVariable implements Variable {
    private int value;

    public IntegerVariable(int value) {
        this.value = value;
    }

    @Override
    public boolean isTrue() {
        return value != 0;
    }

    @Override
    public int asInt() {
        return value;
    }

    @Override
    public VariableList asList() {
        return new VariableList(value);
    }

    @Override
    public VariableType type() {
        return VariableType.INTEGER;
    }

    @Override
    public String toString() {
        return "Integer [" + value + "]";
    }

    @Override
    public String asString() {
        return String.valueOf(value);
    }
}
