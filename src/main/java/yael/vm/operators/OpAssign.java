package yael.vm.operators;

import yael.vm.*;

public class OpAssign extends Operator {
    private final String[] destinations;
    private final Argument[] values;

    public OpAssign(Argument... arguments) {
        super(arguments);

        assert arguments.length % 2 == 0;

        int arraySize = arguments.length / 2;
        destinations = new String[arraySize];
        values = new Argument[arraySize];

        for (int i = 0, j = arraySize; i < arraySize; i++, j++) {
            if (!arguments[j].isIdentifier()) {
                error("Identifier expected, `" + arguments[j] + "` found");
            }

            destinations[i] = arguments[j].identifier();
            values[i] = arguments[i];
        }
    }

    @Override
    public Variable eval(Context ctx) {
        for (int i = 0; i < destinations.length; i++) {
            Variable value = argumentValue(ctx, values[i]);
            ctx.var(destinations[i], value);
        }

        return EmptyVariable.EMPTY_INSTANCE;
    }
}
