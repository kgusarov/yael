package yael.vm.operators;

import yael.vm.*;

public class OpAsInt extends Operator {
    private final Argument arg;

    public OpAsInt(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        return new IntegerVariable(argumentValue(ctx, arg).asInt());
    }
}
