package yael.vm.operators;

import yael.vm.*;

public class OpPrintString extends Operator {
    private Argument[] arguments;

    public OpPrintString(Argument... arguments) {
        super(arguments);

        this.arguments = arguments;
    }

    private void print(Context ctx, Variable v) {
        switch (v.type()) {
            case INTEGER:
                ctx.printChar(v.asInt());
                break;
            case LIST:
                for (Variable var : v.asList()) {
                    print(ctx, var);
                }
                break;
            case STRING:
                ctx.printString(v.asString());
                break;
        }
    }

    @Override
    public Variable eval(Context ctx) {
        for (Argument arg : arguments) {
            Variable value = argumentValue(ctx, arg);
            print(ctx, value);
        }

        return EmptyVariable.EMPTY_INSTANCE;
    }
}
