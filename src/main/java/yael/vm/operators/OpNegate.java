package yael.vm.operators;

import yael.vm.*;

public class OpNegate extends Operator {
    private Argument arg;

    public OpNegate(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        return new IntegerVariable(-argumentValue(ctx, arg).asInt());
    }
}
