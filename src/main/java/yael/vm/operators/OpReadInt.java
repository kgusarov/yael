package yael.vm.operators;

import yael.vm.Argument;
import yael.vm.Context;
import yael.vm.IntegerVariable;
import yael.vm.Variable;

public class OpReadInt extends InputOperator {
    public OpReadInt(Argument... arguments) {
        super(0, arguments);
    }

    @Override
    public Variable eval(Context ctx) {
        return new IntegerVariable(scanner.nextInt());
    }
}
