package yael.vm.operators;

import yael.vm.*;

public class OpList extends Operator {
    private final Argument[] values;

    public OpList(Argument... arguments) {
        super(arguments);

        values = arguments;
    }

    @Override
    public Variable eval(Context ctx) {
        VariableList list = new VariableList();
        for (Argument arg : values) {
            Variable variable = arg.eval(ctx);
            list.add(variable);
        }

        return new ListVariable(list);
    }
}
