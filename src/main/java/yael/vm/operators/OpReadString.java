package yael.vm.operators;

import yael.vm.Argument;
import yael.vm.Context;
import yael.vm.StringVariable;
import yael.vm.Variable;

public class OpReadString extends InputOperator {
    public OpReadString(Argument... arguments) {
        super(0, arguments);
    }

    @Override
    public Variable eval(Context ctx) {
        return new StringVariable(scanner.nextLine());
    }
}
