package yael.vm.operators;

import org.apache.commons.lang3.StringUtils;
import yael.vm.*;

import java.util.ArrayList;
import java.util.List;

public class OpPrint extends Operator {
    private Argument[] arguments;

    public OpPrint(Argument... arguments) {
        super(arguments);

        this.arguments = arguments;
    }

    @Override
    public Variable eval(Context ctx) {
        List<String> strings = new ArrayList<>();
        for (Argument argument : arguments) {
            strings.add(argumentValue(ctx, argument).dump());
        }

        ctx.printString(StringUtils.join(strings, ','));
        return EmptyVariable.EMPTY_INSTANCE;
    }
}
