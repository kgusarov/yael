package yael.vm.operators;

import yael.vm.Argument;
import yael.vm.Context;
import yael.vm.Operator;
import yael.vm.Variable;

public class OpReturn extends Operator {
    private Argument arg;

    public OpReturn(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        return argumentValue(ctx, arg);
    }
}
