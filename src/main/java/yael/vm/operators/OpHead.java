package yael.vm.operators;

import yael.vm.*;

public class OpHead extends Operator {
    private final Argument arg;

    public OpHead(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        VariableList list = argumentValue(ctx, arg).asList();
        if (list.isEmpty()) {
            return EmptyVariable.EMPTY_INSTANCE;
        }

        return list.get(0);
    }
}
