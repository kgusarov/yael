package yael.vm.operators;

import yael.vm.*;

public class OpAt extends Operator {
    private final Argument list;
    private final Argument index;

    public OpAt(Argument... arguments) {
        super(2, arguments);

        list = arguments[0];
        index = arguments[1];
    }

    @Override
    public Variable eval(Context ctx) {
        int index = argumentValue(ctx, this.index).asInt();
        VariableList list = argumentValue(ctx, this.list).asList();

        if ((index < 0) || (list.size() <= index)) {
            return EmptyVariable.EMPTY_INSTANCE;
        }

        return list.get(index);
    }
}
