package yael.vm.operators;

import yael.vm.*;

public class OpSum extends Operator {
    private final Argument[] values;

    public OpSum(Argument... arguments) {
        super(arguments);

        values = arguments;
    }

    private int sum(Variable variable) {
        switch (variable.type()) {
            case STRING:
            case INTEGER:
                return variable.asInt();
            case LIST:
                int sum = 0;
                for (Variable var : variable.asList()) {
                    sum += sum(var);
                }

                return sum;
        }

        return 0;
    }

    @Override
    public Variable eval(Context ctx) {
        int sum = 0;
        for (Argument arg : values) {
            Variable variable = arg.eval(ctx);
            sum += sum(variable);
        }

        return new IntegerVariable(sum);
    }
}
