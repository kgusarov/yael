package yael.vm.operators;

import yael.vm.*;

public class OpPrintChar extends Operator {
    private Argument arg;

    public OpPrintChar(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        Variable value = argumentValue(ctx, this.arg);
        ctx.printChar(value.asInt());
        return EmptyVariable.EMPTY_INSTANCE;
    }
}
