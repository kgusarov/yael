package yael.vm.operators;

import yael.vm.*;

public class OpSetAt extends Operator {
    private final Argument list;
    private final Argument value;
    private final Argument index;

    public OpSetAt(Argument... arguments) {
        super(3, arguments);

        value = arguments[0];
        list = arguments[1];
        index = arguments[2];
    }

    @Override
    public Variable eval(Context ctx) {
        int index = argumentValue(ctx, this.index).asInt();
        Variable value = argumentValue(ctx, this.value);
        VariableList list = argumentValue(ctx, this.list).asList();

        if ((index < 0) || (list.size() < index)) {
            return EmptyVariable.EMPTY_INSTANCE;
        }

        if (index == list.size()) {
            list.add(value);
        } else {
            list.set(index, value);
        }

        if (this.list.isIdentifier()) {
            return argumentValue(ctx, this.list);
        } else {
            return new ListVariable(list);
        }
    }
}
