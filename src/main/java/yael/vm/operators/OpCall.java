package yael.vm.operators;

import yael.vm.*;

public class OpCall extends Operator {
    private String functionName;
    private Argument[] arguments;

    public OpCall(String functionName, Argument... arguments) {
        super(arguments);

        this.functionName = functionName;
        this.arguments = arguments;
    }

    @Override
    public Variable eval(Context ctx) {
        Function f = ctx.unit(functionName);
        return f.eval(ctx, arguments);
    }

    @Override
    protected String getName() {
        return super.getName() + "-" + functionName;
    }
}
