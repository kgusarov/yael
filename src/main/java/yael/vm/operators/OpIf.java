package yael.vm.operators;

import yael.compiler.CompileUnit;
import yael.compiler.StringUtil;
import yael.vm.Argument;
import yael.vm.Context;
import yael.vm.Operator;
import yael.vm.Variable;

public class OpIf extends Operator {
    private final Argument condition;
    private final CompileUnit unit;
    private final CompileUnit elseUnit;

    public OpIf(CompileUnit unit, CompileUnit elseUnit, Argument... arguments) {
        super(1, arguments);

        this.condition = arguments[0];
        this.unit = unit;
        this.elseUnit = elseUnit;
    }

    @Override
    public Variable eval(Context ctx) {
        Variable value = condition.eval(ctx);
        if (value.isTrue()) {
            return unit.eval(ctx);
        } else {
            if (elseUnit != null) {
                return elseUnit.eval(ctx);
            }
        }

        return value;
    }

    @Override
    public String toString(int indent) {
        StringBuilder sb = new StringBuilder(super.toString(indent));
        sb.append(" [\n");

        sb.append(unit.toString(indent + 2));
        sb.append('\n');
        StringUtil.indent(indent + 1, sb);

        if (elseUnit != null) {
            sb.append("] else [\n");
            sb.append(elseUnit.toString(indent + 2));
            sb.append('\n');
            StringUtil.indent(indent + 1, sb);
        }

        sb.append("]\n");

        return sb.toString();
    }
}
