package yael.vm.operators;

import yael.vm.*;

public class OpNot extends Operator {
    private final Argument arg;

    public OpNot(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        Variable var = argumentValue(ctx, arg);
        return var.isTrue() ? EmptyVariable.FALSE_INSTANCE : EmptyVariable.TRUE_INSTANCE;
    }
}
