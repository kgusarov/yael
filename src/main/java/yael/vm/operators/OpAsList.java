package yael.vm.operators;

import yael.vm.*;

public class OpAsList extends Operator {
    private final Argument arg;

    public OpAsList(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        return new ListVariable(argumentValue(ctx, arg).asList());
    }
}
