package yael.vm.operators;

import yael.vm.*;

public class OpRemoveHead extends Operator {
    private Argument arg;

    public OpRemoveHead(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        VariableList list = argumentValue(ctx, arg).asList();
        return list.remove(0);
    }
}
