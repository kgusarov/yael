package yael.vm.operators;

import yael.vm.*;

public class OpAsString extends Operator {
    private final Argument arg;

    public OpAsString(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        return new StringVariable(argumentValue(ctx, arg).asString());
    }
}
