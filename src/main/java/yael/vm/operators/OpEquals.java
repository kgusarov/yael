package yael.vm.operators;

import yael.vm.*;

public class OpEquals extends Operator {
    private final Argument a;
    private final Argument b;

    public OpEquals(Argument... arguments) {
        super(2, arguments);

        a = arguments[0];
        b = arguments[1];
    }

    @Override
    public Variable eval(Context ctx) {
        Variable a = argumentValue(ctx, this.a);
        Variable b = argumentValue(ctx, this.b);

        return equals(a, b);
    }

    private Variable equals(Variable a, Variable b) {
        if (a.type() != b.type()) {
            return EmptyVariable.FALSE_INSTANCE;
        }

        switch (a.type()) {
            case INTEGER:
                return (a.asInt() == b.asInt()) ?
                        EmptyVariable.TRUE_INSTANCE : EmptyVariable.FALSE_INSTANCE;
            case STRING:
                return (a.asString().equals(b.asString())) ?
                        EmptyVariable.TRUE_INSTANCE : EmptyVariable.FALSE_INSTANCE;
            case LIST:
                VariableList listA = a.asList();
                VariableList listB = b.asList();
                if (listA.size() != listB.size()) {
                    return EmptyVariable.FALSE_INSTANCE;
                }

                for (int i = 0; i < listA.size(); i++) {
                    if (!equals(listA.get(i), listB.get(i)).isTrue()) {
                        return EmptyVariable.FALSE_INSTANCE;
                    }
                }

                return EmptyVariable.TRUE_INSTANCE;
        }

        return EmptyVariable.FALSE_INSTANCE;
    }
}
