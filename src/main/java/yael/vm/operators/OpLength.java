package yael.vm.operators;

import yael.vm.*;

public class OpLength extends Operator {
    private final Argument arg;

    public OpLength(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        VariableList list = argumentValue(ctx, arg).asList();
        return new IntegerVariable(list.size());
    }
}
