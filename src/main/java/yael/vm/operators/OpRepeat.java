package yael.vm.operators;

import yael.compiler.CompileUnit;
import yael.compiler.StringUtil;
import yael.vm.*;

public class OpRepeat extends Operator {
    private final Argument times;
    private final CompileUnit unit;

    public OpRepeat(CompileUnit unit, Argument... arguments) {
        super(1, arguments);

        this.times = arguments[0];
        this.unit = unit;
    }

    @Override
    public Variable eval(Context ctx) {
        Variable result = EmptyVariable.EMPTY_INSTANCE;
        int times = argumentValue(ctx, this.times).asInt();

        for (int i = 0; i < times; i++) {
            result = unit.eval(ctx);
        }

        return result;
    }

    @Override
    public String toString(int indent) {
        StringBuilder sb = new StringBuilder(super.toString(indent));
        sb.append(" [\n");
        sb.append(unit.toString(indent + 2));

        sb.append('\n');
        StringUtil.indent(indent + 1, sb);
        sb.append("]");

        return sb.toString();
    }
}
