package yael.vm.operators;

import yael.vm.*;

public class OpRemoveLast extends Operator {
    private Argument arg;

    public OpRemoveLast(Argument... arguments) {
        super(1, arguments);

        arg = arguments[0];
    }

    @Override
    public Variable eval(Context ctx) {
        VariableList list = argumentValue(ctx, arg).asList();
        if (list.isEmpty()) {
            return EmptyVariable.EMPTY_INSTANCE;
        }

        return list.remove(list.size() - 1);
    }
}
