package yael.vm.operators;

import yael.vm.*;

public class OpAdd extends Operator {
    private final Argument list;
    private final Argument value;

    public OpAdd(Argument... arguments) {
        super(2, arguments);

        value = arguments[0];
        list = arguments[1];
    }

    @Override
    public Variable eval(Context ctx) {
        Variable value = argumentValue(ctx, this.value);
        VariableList list = argumentValue(ctx, this.list).asList();

        list.add(value);

        if (this.list.isIdentifier()) {
            return argumentValue(ctx, this.list);
        } else {
            return new ListVariable(list);
        }
    }
}
