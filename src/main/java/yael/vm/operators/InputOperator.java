package yael.vm.operators;

import yael.vm.Argument;
import yael.vm.Operator;

import java.util.Scanner;

abstract class InputOperator extends Operator {
    protected static final Scanner scanner = new Scanner(System.in);

    public InputOperator(Argument... arguments) {
        super(arguments);
    }

    public InputOperator(int expected, Argument... arguments) {
        super(expected, arguments);
    }
}
