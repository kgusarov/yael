package yael.vm;

import yael.compiler.StringUtil;

import java.util.Arrays;

public abstract class Operator {
    protected final Argument[] arguments;

    protected Operator(int expected, Argument... arguments) {
        this.arguments = arguments;
        if (arguments.length != expected) {
            error("Expected " + expected + " arguments for " + getName());
        }
    }

    protected Operator(Argument... arguments) {
        this.arguments = arguments;
    }

    public abstract Variable eval(Context ctx);

    @Override
    public String toString() {
        return toString(0);
    }

    public String toString(int indent) {
        StringBuilder sb = new StringBuilder();
        StringUtil.indent(indent, sb);

        sb.append(getName()).append(" [arguments=");
        sb.append(Arrays.toString(arguments)).append("]");

        return sb.toString();
    }

    protected String getName() {
        return getClass().getSimpleName();
    }

    protected final Variable argumentValue(Context ctx, Argument argument) {
        return argument.isIdentifier() ? ctx.var(argument.identifier()) : argument.eval(ctx);
    }

    protected final void error(String text) {
        throw new IllegalArgumentException(text);
    }
}
