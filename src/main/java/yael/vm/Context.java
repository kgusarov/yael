package yael.vm;

import yael.compiler.CompileUnit;

public interface Context {
    Variable var(String name);

    void var(String name, Variable value);

    Context global();

    Context parentContext();

    <T extends CompileUnit> T unit(String name);

    default void invalidUnit(String name) {
        throw new IllegalStateException("Invalid unit: `" + name + "`");
    }

    default void printChar(int i) {
        char c = (char) i;
        System.out.print(c);
    }

    default void printString(String s) {
        System.out.print(s);
    }

    default Variable eval(CompileUnit unit) {
        Variable result = EmptyVariable.EMPTY_INSTANCE;
        for (Operator operator : unit.operators()) {
            result = operator.eval(this);
        }

        return result;
    }
}