package yael.vm;

public class EmptyVariable implements Variable {
    public static final EmptyVariable EMPTY_INSTANCE = new EmptyVariable(false);
    public static final EmptyVariable TRUE_INSTANCE = new EmptyVariable(true);
    public static final EmptyVariable FALSE_INSTANCE = new EmptyVariable(false);

    private final boolean isTrue;

    private EmptyVariable(boolean isTrue) {
        this.isTrue = isTrue;
    }

    @Override
    public boolean isTrue() {
        return isTrue;
    }

    @Override
    public int asInt() {
        return 0;
    }

    @Override
    public VariableList asList() {
        return new VariableList();
    }

    @Override
    public VariableType type() {
        return VariableType.INTEGER;
    }

    @Override
    public String toString() {
        return "<EMPTY: " + isTrue + ">";
    }

    @Override
    public String asString() {
        return "";
    }
}
