package yael.vm;

import org.apache.commons.lang3.StringEscapeUtils;

public class StringVariable implements Variable {
    private String value;

    public StringVariable(String value) {
        this.value = value;
    }

    @Override
    public boolean isTrue() {
        return !value.isEmpty();
    }

    @Override
    public int asInt() {
        return 0;
    }

    @Override
    public VariableList asList() {
        VariableList result = new VariableList();
        for (int i = 0; i < value.length(); i++) {
            result.add(new IntegerVariable(value.charAt(i)));
        }

        return result;
    }

    @Override
    public VariableType type() {
        return VariableType.STRING;
    }

    @Override
    public String toString() {
        return "String [\"" + StringEscapeUtils.escapeJava(value) + "\"]";
    }

    @Override
    public String asString() {
        return value;
    }
}
