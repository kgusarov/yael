package yael.vm;

import java.util.ArrayList;
import java.util.Collections;

public class VariableList extends ArrayList<Variable> {
    private static final long serialVersionUID = 4601134783674887670L;

    public VariableList(Variable... elements) {
        Collections.addAll(this, elements);
    }

    public VariableList(Integer... elements) {
        for (Integer integer : elements) {
            add(new IntegerVariable(integer));
        }
    }

    public VariableList(VariableList other) {
        super(other);
    }

    public VariableList() {
    }
}
