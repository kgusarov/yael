package yael.vm;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.stream.Collectors;

public interface Variable {
    boolean isTrue();

    int asInt();

    String asString();

    VariableList asList();

    VariableType type();

    default String dump() {
        switch (type()) {
            case INTEGER:
                return String.valueOf(asInt());
            case LIST:
                StringBuilder sb = new StringBuilder("(");
                sb.append(StringUtils.join(asList().stream().map(v -> v.dump()).collect(Collectors.toList()), ','));
                return sb.append(')').toString();
            case STRING:
                return "\"" + StringEscapeUtils.escapeJava(asString()) + "\"";
        }

        return "";
    }
}
