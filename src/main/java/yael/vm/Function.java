package yael.vm;

import yael.compiler.CompileUnit;

import java.util.List;

public final class Function extends CompileUnit {
    private final List<String> argumentNames;

    public Function(String name, List<String> argumentNames) {
        super(name);
        this.argumentNames = argumentNames;
    }

    public Variable eval(Context parent, Argument... arguments) {
        FunctionContext ctx = new FunctionContext(parent, this);

        for (int i = 0; i < argumentNames.size(); i++) {
            String name = argumentNames.get(i);

            if (i >= arguments.length) {
                ctx.variables.put(name, EmptyVariable.EMPTY_INSTANCE);
            } else {
                ctx.variables.put(name, arguments[i].eval(parent));
            }
        }

        for (Operator o : operators) {
            ctx.result = o.eval(ctx);
        }

        return ctx.result;
    }

    @Override
    public String getName() {
        return super.getName() + argumentNames;
    }
}
