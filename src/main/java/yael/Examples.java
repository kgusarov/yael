package yael;

import yael.compiler.Compiler;
import yael.compiler.Program;
import yael.vm.VM;

public class Examples {
    public static void main(String[] args) {
        compileAndRun("HelloWorld.yael");
        compileAndRun("HelloWorldSimple.yael");
        compileAndRun("HelloWorldSimpler.yael");
        compileAndRun("HelloWorldList.yael");
        compileAndRun("HelloWorldString.yael");
        compileAndRun("WhileTest.yael");
        compileAndRun("ListTest.yael");
        compileAndRun("PrintTest.yael");
        compileAndRun("EqualsTest.yael");
        compileAndRun("NegateTest.yael");
        compileAndRun("BrainFuckInterpreter.yael");
        compileAndRun("FirstLastTest.yael");
        compileAndRun("AsTest.yael");
        compileAndRun("AtTest.yael");
        compileAndRun("AddTest.yael");
        compileAndRun("LengthTest.yael");
        compileAndRun("Sum2Numbers.yael");
        compileAndRun("Cat.yael");
    }

    private static void compileAndRun(String file) {
        System.out.println("===================================================================");
        System.out.println(file);
        System.out.println("===================================================================");

        Program program = Compiler.compile(file, false);
        System.out.println(program);

        VM vm = new VM(program);
        vm.run();
        System.out.println(vm);
    }
}
