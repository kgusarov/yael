grammar YAEL;

import integer, identifier, string;

program
	:	compileUnit* EOF
	;

compileUnit
	:	function
	|	block
	;

callableBlock
	:	block
	;

argumentNameList
	:	'(' variable* ')'
	;

function
	:	'{function-' functionName '-with-arguments-' argumentNameList '-is:' compileUnit* '}'
	;

functionName
	:	Identifier
	;

block
	:	'{' operator* '}'
	;

operator
	:	assign
	|	printChar
	| 	call
	|	returnOperator
	|	ifOperator
	|	repeat
	|	printString
	|	sum
	|	negate
	|	whileOperator
	|	list
	|	head
	|	print
	|	removeHead
	|	equals
	|	asString
	|	asInt
	|	asList
	|	readInt
	|	readString
	|	at
	|	global
	|	setAt
	|	last
	|	removeLast
	|	add
	|	length
	|	not
	;

not
	:	'{not-' argumentList '}'
	;

length
	:	'{length-' argumentList '}'
	;

add
	:	'{add-' argumentList '-to-' argumentList '}'
	;

last
	:	'{last-' argumentList '}'
	;

removeLast
	:	'{remove-last-' argumentList '}'
	;

global
	:	'{assign-global-' argumentList '-to-' targetVariableList '}' 
	;

setAt
	:	'{set-value-' argumentList '-into-' argumentList '-at-index-' argumentList '}'
	;

at
	:	'{get-value-from-' argumentList '-at-index-' argumentList '}'
	;

readInt
	:	'{read-int}'
	;

readString
	:	'{read-string}'
	;

asString
	:	'{as-string-' argumentList '}'
	;

asInt
	:	'{as-int-' argumentList '}'
	;
	
asList
	:	'{as-list-' argumentList '}'
	;

equals
	:	'{equals-' argumentList '-to-' argumentList '}'
	;

whileOperator
	:	'{while-(' expression ')-do-' callableBlock '}'
	;

print
	:	'{print-' argumentList '}'
	;

sum
	:	'{sum-' argumentList '}'
	;

negate
	:	'{negate-' argumentList '}'
	;

repeat
	:	'{repeat-' argumentList '-times-' callableBlock '}'
	;

ifOperator
	:	'{if-(' expression ')-then-' callableBlock '}'
	|	'{if-(' expression ')-then-' callableBlock '-else-' callableBlock '}'
	;

targetVariable
	:	variable
	;

head
	:	'{first-' argumentList '}'
	;

removeHead
	:	'{remove-first-' argumentList '}'
	;

list
	:	'{list-' argumentList '}'
	;

assign
	:	'{assign-' argumentList '-to-' targetVariableList '}' 
	;

printChar
	:	'{print-char-' argumentList '}'
	;

printString
	:	'{print-string-' argumentList '}'
	;

call
	:	'{call-function-' functionName '-with-arguments-' argumentList '}'
	;

returnOperator
	:	'{return-' argumentList '}'
	;

argumentList
	:	'(' expression* ')'
	;

targetVariableList
	:	'(' targetVariable* ')'
	;

expression
	:	operator
	|	variable
	|	integer
	|	string
	;

argument
	:	variable
	|	integer
	|	string
	;

variable
	:	Identifier
	;

integer
	:	SignedInteger
	|	UnsignedInteger
	;

string
	:	StringLiteral
	;

WhiteSpace
	:	[ \t\r\n\u000C]+ -> skip
	;