lexer grammar string;

fragment
EscapeSequence
	:	'\\' [btnfr"'\\]
	;

fragment
StringCharacters
	:	StringCharacter+
	;

fragment
StringCharacter
	:	~["\\]
    |	EscapeSequence
	;

StringLiteral
	:	'"' StringCharacters? '"'
	;