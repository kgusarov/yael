lexer grammar identifier;

fragment
IdentifierLetter
	:	[a-zA-Z]
	;

fragment
IdentifierLetterOrDigit
	:	[a-zA-Z0-9]
	;

Identifier
	:	IdentifierLetter IdentifierLetterOrDigit*
	;
