lexer grammar integer;

fragment 
Sign
	:	[+-]
	;

fragment 
Digit
	:	'0'
	|	NonZeroDigit
	;

fragment
NonZeroDigit
	:	[1-9]
	;

fragment
Digits
	:	Digit Digit*
	;

fragment
Numeral
	:	'0'
	|	NonZeroDigit Digits*
	;

SignedInteger
	:	Sign Numeral
	;

UnsignedInteger
	:	Numeral
	;
